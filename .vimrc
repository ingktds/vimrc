"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=/Users/ingktds/.cache/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('/Users/ingktds/.cache/dein/')
  call dein#begin('/Users/ingktds/.cache/dein/')

  " Let dein manage dein
  " Required:
  call dein#add('/Users/ingktds/.cache/dein/repos/github.com/Shougo/dein.vim')

  " Add or remove your plugins here:
  call dein#add('Shougo/neosnippet.vim')
  call dein#add('Shougo/neosnippet-snippets')
  call dein#add('Shougo/neocomplete.vim')
  call dein#add('thinca/vim-quickrun')
  call dein#add('Yggdroot/indentLine')
  call dein#add('davidhalter/jedi-vim')
  call dein#add('kevinw/pyflakes')
  call dein#add('vim-scripts/AnsiEsc.vim')
  call dein#add('kannokanno/previm')
  call dein#add('tyru/open-browser.vim')
  call dein#add('Shougo/vimproc.vim', {'build' : 'make'})
  call dein#add('vim-syntastic/syntastic')
  call dein#add('fatih/vim-go')
  call dein#add('elzr/vim-json')
  call dein#add('xavierchow/vim-swagger-preview')
  call dein#add('posva/vim-vue')
  call dein#add('justmao945/vim-clang')
  "call dein#add('neoclide/coc.nvim', {'merged':0, 'rev': 'release'})

  " You can specify revision/branch/tag.
  " call dein#add('Shougo/vimshell', { 'rev': '3787e5' })

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif

"End dein Scripts-------------------------

set number
set ruler
set splitright
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
set autoindent
set smartindent
"set textwidth=80
set backspace=indent,eol,start
set binary noeol

let g:jedi#rename_command = ""
let g:jedi#force_py_version = 3

augroup MyVimrc
    autocmd!
augroup END
autocmd MyVimrc BufNewFile,BufRead *Test.php setlocal ft=php.phpunit
if !exists("g:quickrun_config")
    let g:quickrun_config = {}
endif

let g:quickrun_config._ = {
\ "runner": "vimproc",
\}

let g:quickrun_config['php.phpunit'] = {
\ 'command'                        : 'vendor/bin/phpunit',
\ 'cmdopt'                         : '--colors=always',
\}

augroup quickrun
    autocmd!
    autocmd FileType quickrun AnsiEsc
augroup END

" previm setting
augroup PrevimSettings
    autocmd!
    autocmd BufNewFile,BufRead *.{md,mdwn,mkd,mkdn,mark*} set filetype=markdown
augroup END

" filetype indent setting
augroup filetypeindent
    autocmd!
    autocmd BufNewFile,BufRead *.{html,rb,yml,tmpl,epl,erb} setlocal tabstop=2 softtabstop=2 shiftwidth=2
augroup END

let g:syntastic_python_checkers = ["flake8"]

" vim-go settings
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_metalinter_autosave = 1
let g:go_fmt_command = "goimports"
autocmd FileType go nmap <leader>t  <Plug>(go-test)

" vim-json settings
let g:vim_json_syntax_conceal = 0

" neoconplete settings
let g:neocomplete#enable_at_startup = 1
if !exists('g:neocomplete#sources#omni#input_patterns')
      let g:neocomplete#sources#omni#input_patterns = {}
      endif
let g:neocomplete#sources#omni#input_patterns.go = '\h\w\.\w*'

augroup titlesettings
  autocmd!
  autocmd BufEnter * call system("tmux rename-window " . "'[vim] " . expand("%:t") . "'")
  autocmd VimLeave * call system("tmux rename-window zsh")
  autocmd BufEnter * let &titlestring = ' ' . expand("%:t")
augroup END

